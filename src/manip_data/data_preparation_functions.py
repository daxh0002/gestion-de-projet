# Import librarys from the parent directory
import sys
import os
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir)
sys.path.append('../')
from library import *



def download_csv(url, filename):
    """
    Downloads a CSV file from a given URL and saves it to a specified file.

    Args:
    url (str): The URL from which the CSV file will be downloaded.
    filename (str): The name of the file where the CSV content will be saved locally.

    This function sends a GET request to the provided URL, checks if the request was successful, 
    and if so, writes the content of the response to a file on the local disk.
    If the request fails, an exception is raised with the corresponding error code.
    """

    # Sending a GET request to the provided URL to fetch the file content
    response = requests.get(url)

    # Checking if the HTTP request was successful (status code 200)
    if response.status_code == 200:
        # If the request is successful, open the file in binary write mode ('wb')
        # and write the content of the response to the specified file
        with open(filename, 'wb') as f:
            f.write(response.content)

        # Print a success message indicating the file has been downloaded
        print(f"Fichier téléchargé : {filename}")
    else:
        # If the request fails (non-200 status code), raise an exception with the error code
        raise Exception(f"Erreur lors du téléchargement du fichier : {response.status_code}")


def load_csv_data(filename):
    """
    Loads a CSV file as a Pandas DataFrame.

    Args:
    filename (str): The path to the CSV file to load.

    Returns:
    pandas.DataFrame: The DataFrame containing the loaded data.
    """
    # Check if the file exists
    if not os.path.exists(filename):
        raise FileNotFoundError(f"Le fichier {filename} n'existe pas.")

    # Read the CSV file with Pandas
    df = pd.read_csv(filename, sep=';', header=0)
    print("Aperçu des données importées:")
    print(df.head())
    print("\nInformations sur les données:")
    print(df.info())

    return df

def clean_data_with_absence_check(df: pd.DataFrame) -> pd.DataFrame:
    """
    Cleans the DataFrame by adding a 'va_abs' column indicating the presence of missing values,
    and filtering rows based on specific conditions.

    Steps:
    1. Replaces commas with dots in the specified columns and converts them to float.
    2. Creates a 'va_abs' column that takes the value 1 if DIST, DUREE, or CO2_HEBDO are missing,
       otherwise it takes the value 0.
    3. Calculates the count of rows with 'va_abs' equal to 0 or 1.
    4. Calculates the percentage of 0 and 1 in 'va_abs'.
    5. Filters the DataFrame to keep only the rows where 'va_abs' is 0 and CO2_HEBDO > 0.
    6. Drops the 'va_abs' and 'CARBU_HEBDO' columns from the DataFrame.

    Args:
        df (pd.DataFrame): The DataFrame containing the data to clean.

    Returns:
        pd.DataFrame: The cleaned DataFrame.
    """
    # Step 1: Replace commas with dots and convert to float
    for column in ['DIST_HEBDO', 'CARBU_HEBDO', 'IPONDI', 'DIST', 'DUREE', 'CO2_HEBDO']:
        if column in df.columns:
            df[column] = df[column].replace(',', '.', regex=True).astype(float, errors='ignore')

    print("Schéma des données après conversion :")
    print(df.info())

    # Step 2: Create the 'va_abs' column
    df['va_abs'] = np.where(
        df[['DIST', 'DUREE', 'CO2_HEBDO']].isnull().any(axis=1),
        1,
        0
    )

    # Step 3: Calculate the count of 0s and 1s in 'va_abs'
    counts = df['va_abs'].value_counts()

    # Step 4: Calculate the percentages of 0 and 1
    total_count = len(df)
    percentages = counts / total_count * 100

    print("\nNombre de 0 et de 1 :")
    print(counts)

    print("\nPourcentage de 0 et de 1 :")
    for value, percentage in percentages.items():
        print(f"{value}: {percentage:.2f}%")

    # Step 5: Filter the DataFrame to keep only rows where va_abs is 0 and CO2_HEBDO > 0
    filtered_df = df[(df['va_abs'] == 0) & (df['CO2_HEBDO'] > 0)]

    # Step 6: Drop the 'va_abs' and 'CARBU_HEBDO' columns
    filtered_df = filtered_df.drop(columns=['va_abs'], errors='ignore')
    filtered_df = filtered_df.drop(columns=['CARBU_HEBDO'], errors='ignore')

    return filtered_df

variables_a_convertir = [
    'LIEU_RESID', 'LIEU_TRAV', 'MODTRANS', 'AGEREVQ', 'CS1', 'DIPL', 'EMPL', 
    'ILT', 'ILTUU', 'IMMI', 'INATC', 'INEEM', 'INPOM', 'MOCO', 'NA5', 'NPERR', 
    'SEXE', 'STAT', 'STOCD', 'TP', 'TYPL', 'TYPMR', 'VOIT', 'DEP_RESID', 
    'REG_RESID', 'CATEAAV20_RESID', 'TAAV2017_RESID', 'GRILLE_DENSITE_RESID', 
    'EPCI_EPT_RESID', 'ZE2020_RESID', 'AAV20_RESID', 'DEP_TRAV', 'REG_TRAV', 
    'CATEAAV20_TRAV', 'TAAV2017_TRAV', 'GRILLE_DENSITE_TRAV', 'EPCI_EPT_TRAV', 
    'ZE2020_TRAV', 'AAV20_TRAV'
]

def convert_to_category(df, variables = variables_a_convertir, types = 'category'):
    """
    This function converts the variables in 'variables' to the type stored in 'types'.

    Parameters
    ----------
    df : DataFrame
        DESCRIPTION : The DataFrame to be modified.
    variables : list, optional
        DESCRIPTION : The list of variables to convert.
        The default is variables_to_convert.
    types : str, optional
        DESCRIPTION : The type to which the variables should be converted.
        The default is 'category'.

    Returns
    -------
    None.

    """

    for var in variables:
        if var in df.columns:
            df[var] = df[var].astype(types)
        else:
            print(f"La variable '{var}' n'existe pas dans le DataFrame.")



def select_random(csv_file, sample_size=300000, chunk_size=100000):
    """
    Functionality: The function will create chunks of 100,000 individuals by default.
    It will assign each individual an index between 0 and the total number of rows in the CSV (n)
    randomly.
    If the index value is less than sample_size, then the individual is added to the DataFrame.
    It continues this way until it obtains a DataFrame with a number of individuals equal to
    sample_size.
    At the end, the function returns a DataFrame with a number of indices equal to sample_size.
    It also deletes variables that will no longer be used to free up memory.
    
    Parameters
    ----------
    csv_file : TYPE : str
        DESCRIPTION: Path to the CSV file to be processed
    sample_size : TYPE, optional
        DESCRIPTION. The default is 300000.
        Final sample size
    chunk_size : TYPE, optional
        DESCRIPTION. The default is 100000.
        Chunk size

    Returns
    -------
    TYPE DataFrame
        DESCRIPTION. DataFrame containing the selected values

    """

    sampled_rows = []  # List to store sampled rows

    # Count the total number of rows for iterations
    total_rows = sum(1 for _ in open(csv_file)) - 1  # -1 to ignore the header

    # Adjust sample_size if total_rows is less than sample_size
    if total_rows < sample_size:
        sample_size = total_rows

    # Count the total number of chunks
    total_chunks = sum(1 for _ in pd.read_csv(csv_file, chunksize=chunk_size, low_memory=False))

    # Read the file in chunks with a progress bar
    with tqdm(total=total_chunks, desc="Séléction des individus") as pbar:
        for chunk in pd.read_csv(csv_file, chunksize=chunk_size, low_memory=False):
            # Generate a random number of indices between 0 and total_rows
            random_indices = random.sample(range(total_rows), len(chunk))

            # Keep rows where the index is less than sample_size
            for i, row in enumerate(chunk.itertuples(index=False)):
                if random_indices[i] < sample_size:
                    sampled_rows.append(row)

            # Update the progress bar
            pbar.update(1)

    # Limit the number of samples to sample_size
    if len(sampled_rows) > sample_size:
        sampled_rows = random.sample(sampled_rows, sample_size)

    # Convert the list of rows to a DataFram
    sampled_df = pd.DataFrame(sampled_rows)

    # Free memory
    del sampled_rows
    del random_indices
    del chunk

    return sampled_df.reset_index(drop=True)

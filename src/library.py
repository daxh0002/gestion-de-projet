# library.py
import pandas as pd
import random
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import math
from tqdm import tqdm
import unittest 
import os
from unittest.mock import patch, MagicMock, mock_open
import shutil
import requests
import sqlite3
from sklearn.tree import DecisionTreeRegressor
from skl2onnx import convert_sklearn
from skl2onnx.common.data_types import FloatTensorType
import onnxruntime as rt
# Import librarys from the parent directory
import sys
import os
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir)
sys.path.append('../')
from library import *

def barplot_csp_and_age(df, highlight_cs1, age_user):
    """
    This function creates two barplots displaying:
    - Average weekly CO2 emissions and home-work distance per socio-professional category.
    - Average weekly CO2 emissions and home-work distance per age group.
    Both graphs are displayed on the same figure, side by side.

    Parameters
    ----------
    df: The dataframe containing the data.
    highlight_cs1: (type: str) The socio-professional category to highlight.
    age_user: (type: str) The age range to highlight.

    Returns
    -------
    None.
    """
    # Group by socio-professional categories (CS1)
    df_grouped = df.groupby('CS1').agg({'CO2_HEBDO': 'mean', 'DIST': 'mean'})
    cs1_categories = {
        1: "Agriculteurs exploitants",
        2: "Artisans/commerçants/chefs d'entreprise",
        3: "Cadres et professions intellectuelles supérieures",
        4: "Professions intermédiaires",
        5: "Employés",
        6: "Ouvriers"
    }
    df_grouped.index = df_grouped.index.map(cs1_categories)

    # Group by age groups (AGEREVQ)
    df_pandas = df.groupby('AGEREVQ').agg({'CO2_HEBDO': 'mean', 'DIST': 'mean'})
    tranches_ages = {
        15: "15-19 ans", 20: "20-24 ans", 25: "25-29 ans", 30: "30-34 ans", 35: "35-39 ans",
        40: "40-44 ans", 45: "45-49 ans", 50: "50-54 ans", 55: "55-59 ans", 60: "60-64 ans",
        65: "65-69 ans",
    }
    df_pandas.index = df_pandas.index.map(tranches_ages)

    # Define bar width
    bar_width = 0.4
    r1_csp = np.arange(len(df_grouped))  # Position for CO2 bars (CSP)
    r2_csp = [x + bar_width for x in r1_csp]  # Position for distance bars (CSP)

    r1_age = np.arange(len(df_pandas))  # Position for CO2 bars (Age)
    r2_age = [x + bar_width for x in r1_age]  # Position for distance bars (Age)

    # Create a figure with 2 subplots (side by side)
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(14, 6))  # 1 row, 2 columns

    # Barplot for CSP
    color = 'skyblue'
    ax1.bar(
        r1_csp,
        df_grouped['CO2_HEBDO'],
        color=color,
        width=bar_width,
        label='CO2 hebdomadaire par personne moyen (en g)'
        )
    ax1.set_xlabel('Catégorie socio-professionnelle')
    ax1.set_ylabel('CO2 hebdomadaire par personne moyen (en g)', color=color)
    ax1.tick_params(axis='y', labelcolor=color)
    ax1.set_xticks([r + bar_width / 2 for r in range(len(df_grouped))])
    ax1.set_xticklabels(df_grouped.index, rotation=45, ha='right')

    ax2_csp = ax1.twinx()
    color = 'lightcoral'
    ax2_csp.bar(
        r2_csp,
        df_grouped['DIST'],
        color=color,
        width=bar_width,
        alpha=0.6,
        label='Distance domicile-travail moyenne (en km)'
        )
    ax2_csp.set_ylabel('Distance domicile-travail moyenne (en km)', color=color)
    ax2_csp.tick_params(axis='y', labelcolor=color)

    # Highlight specified CS1 category
    highlight_index_csp = df_grouped.index.get_loc(highlight_cs1)
    ax1.patches[highlight_index_csp].set_color('blue')
    ax2_csp.patches[highlight_index_csp].set_color('purple')

    # Title for the first graph (CSP)
    ax1.set_title('CO2 et Distance Domicile-Travail pour chaque CSP')

    # Barplot for Age
    color = 'skyblue'
    ax2.bar(
        r1_age,
        df_pandas['CO2_HEBDO'],
        color=color,
        width=bar_width,
        label='CO2 hebdomadaire par personne moyen (en g)'
        )
    ax2.set_xlabel('Tranche d\'âge')
    ax2.set_ylabel('CO2 hebdomadaire par personne moyen (en g)', color=color)
    ax2.tick_params(axis='y', labelcolor=color)
    ax2.set_xticks([r + bar_width / 2 for r in range(len(df_pandas))])
    ax2.set_xticklabels(df_pandas.index, rotation=45, ha='right')

    ax2_age = ax2.twinx()
    color = 'lightcoral'
    ax2_age.bar(
        r2_age,
        df_pandas['DIST'],
        color=color,
        width=bar_width,
        alpha=0.6,
        label='Distance domicile-travail moyenne (en km)'
        )
    ax2_age.set_ylabel('Distance domicile-travail moyenne (en km)', color=color)
    ax2_age.tick_params(axis='y', labelcolor=color)

    # Highlight specified age group
    highlight_index_age = df_pandas.index.get_loc(age_user)
    ax2.patches[highlight_index_age].set_color('blue')
    ax2_age.patches[highlight_index_age].set_color('purple')

    # Title for the second graph (Age)
    ax2.set_title('CO2 et Distance Domicile-Travail pour chaque tranche d\'âge')

    # Adjust layout and display
    fig.tight_layout()  # Prevent overlapping
    plt.show()



def barplot_dist(df, dist_user):

    """
    This function creates a barplot displaying the average weekly CO2 emissions per person
    as a function of home-work distance,
    with the user-given distance displayed in a different color.

    Parameters
    ----------
    df: The dataframe containing the data.
    dist_user: (type : int) The home-work distance given by the user to highlight.
    
    Returns
    -------
    None.
    """
    # Transform the distance given by the user into slices:
    dist_user = math.floor(dist_user / 10) * 10
    dist_user = f"{dist_user}-{dist_user+10}"


    # Create a new column for distance brackets (10 km bracket)
    max_dist = int(df['DIST'].max() // 10) * 10
    bins = range(0, max_dist + 10, 10)
    labels = [f"{i}-{i+10}" for i in bins[:-1]]

    df['distance_bin'] = pd.cut(df['DIST'], bins=bins, labels=labels)

    # Calculate the mean of CO2_hebdo per distance bracket
    df_grouped = df.groupby('distance_bin').agg({'CO2_HEBDO': 'mean'})

    # Revert distance_bin as column
    df_grouped = df_grouped.reset_index()

    # Plot the barplot for the means
    plt.figure(figsize=(12, 6))
    sns.barplot(x='distance_bin', y='CO2_HEBDO', data=df_grouped, palette='Blues')

    # Highlight the specified distance range
    highlight_mask = df_grouped['distance_bin'] == dist_user
    highlight_index = df_grouped[highlight_mask].index[0]
    plt.bar(highlight_index, df_grouped['CO2_HEBDO'].loc[highlight_mask].iloc[0], color='purple')

    # Add labels and a title
    plt.xlabel('Distance domicile-travail (en km)')
    plt.ylabel('Quantité moyenne de CO₂ hebdomadaire (en g)')
    plt.title(
        'Quantité moyenne de CO₂ hebdomadaire par personne selon la distance domicile-travail'
        )

    # Show chart
    plt.tight_layout()
    plt.show()

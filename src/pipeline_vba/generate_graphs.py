#Import librarys from the parent directory
import sys
import os
os.chdir(os.path.dirname(os.path.abspath(__file__)))
sys.path.append('../')
from library import *

from visualisation.co2_analysis_visualization import barplot_csp_and_age

def main():
    """
    This function is designed to handle the entire process of loading user data from a temporary
    CSV file, loading global data from an SQLite database, and using the user’s
    data (age and socio-professional category) to generate a comparison graph with the global data.
    The graph generated allows a visual comparison based on the user’s inputs and the overall
    dataset, providing insight into how the user's data aligns with global trends.

    The process is broken down into several steps:
    1. The function reads user data (age and socio-professional category) from a CSV file.
    2. It connects to an SQLite database to retrieve a subset of global data.
    3. It processes this data and generates a visual graph using the most recent data from the user.

    Parameters
    ----------
    No explicit parameters are passed to this function. However, the paths to the temporary
    CSV file containing user data and the SQLite database containing the global data must be
    hardcoded or specified in the script.

    Returns
    -------
    No direct return. The function prints status messages and generates visual outputs (graphs).
    """

    # Specify the path to the temporary user data file.
    temp_file_path = r"..\..\data\donnees_utilisateur.csv"
    print(f"Chemin du fichier temporaire reçu : {temp_file_path}")

    # Load user data from the CSV file
    try:
        df = pd.read_csv(temp_file_path, header=None, names=["Age", "CSP"], encoding='latin1')
        print("Données utilisateur chargées :")
        print(df.head())
    except Exception as e:
        print(f"Erreur lors du chargement du fichier temporaire : {e}")
        sys.exit(1)

    # Load global data from the SQLite database
    try:
        conn = sqlite3.connect(r"../../data/co2_data.db")
        query = "SELECT * FROM co2_data WHERE ROWID % 20 = 0"  # Retrieve 5% of the rows
        df_total = pd.read_sql_query(query, conn)
        conn.close()
        print(f"Échantillon chargé avec {len(df_total)} lignes.")
    except Exception as e:
        print(f"Erreur lors du chargement des données globales : {e}")
        sys.exit(1)

    # Generate graphs based on the user’s data and global data
    try:
        latest_age = df['Age'].iloc[0]  # Use the most recent age value from the user data
        latest_csp = df['CSP'].iloc[0]  # Use the most recent CSP value from the user data
        # Call the function that generates the combined barplots
        barplot_csp_and_age(df_total, latest_csp, latest_age)
    except Exception as e:
        print(f"Erreur lors du chargement des données globales : {e}")
        sys.exit(1)

if __name__ == "__main__":
    main()

#Import librarys
import sqlite3
import pandas as pd

# Load the complete CSV file
csv_file = r"../../data/co2_domicile_travail.csv"  # Path to the CSV file containing the data
db_file = r"../../data/co2_data.db"  # Path to the SQLite database where the data will be stored

# Connect to the SQLite database
conn = sqlite3.connect(db_file)

# Read the CSV file into a pandas DataFrame
df = pd.read_csv(csv_file, low_memory=False)

# Convert the DataFrame to an SQLite table
df.to_sql("co2_data", conn, if_exists="replace", index=False)

# Close the connection to the database after the data has been saved
conn.close()

# Print a message to confirm that the data has been converted successfully
print("Données converties en SQLite.")

# Import functions from other files and library
from select_and_convert import (
    select_random,
    convert_to_category,
    remove_high_variance_categories,
    convert_to_dummies,
    drop_variable,
)
from lasso_building import train_lasso_model_non_negative
from library import*

class lasso_model:
    """
    A class used to represent a Regression Tree Model.

    Attributes
    ----------
    filepath : str
        The path to the file containing the data.
    taille : int
        The size of the dataset sample.
    df : pandas.DataFrame or None
        The dataframe containing the dataset, initialized as None.
    var_explicative : list or None
        The list of explanatory variables, initialized as None.
    var_explique : str or None
        The dependent variable, initialized as None.


    Methods
    -------
    __init__(self, filepath, taille)
        Initializes the Lasso model with the given file path and size.
    """

    def __init__(self, filepath, taille):
        self.filepath = filepath
        self.taille = taille
        self.df = None
        self.var_explicative = None
        self.var_explique = None

    def load_data(self):
        """step 1 : import a random sample of the data"""
        self.df = select_random(self.filepath, self.taille)


    def preprocess_data(self):
            """Step 2 : Preprocessing of the data"""
            convert_to_category(self.df)
            self.df = remove_high_variance_categories(self.df)

            # Drop columns that are not needed
            self.df = self.df.drop(columns=['CHAMP_CO2', 'DIST_HEBDO', 'DUREE'])
            self.df = drop_variable(self.df)

            # Convert to dummies
            self.df = convert_to_dummies(self.df)

    def split_data(self):
        """Step 3 : Split the data into explanatory and dependent variables"""
        self.var_explicative = self.df.drop(columns=['CO2_HEBDO'])
        self.var_explique = self.df['CO2_HEBDO']

    def train_model(self):
        """Step 4 : Train the Lasso model"""
        rmse, mae, y_test, y_pred = train_lasso_model_non_negative(
            self.var_explique,
            self.var_explicative,
            'modele_lasso'
        )
        print(f"RMSE pour le modèle Lasso : {rmse}")
        #6335.33
        print(f"MAE pour le modèle Lasso : {mae}")
        #3453.23

    def run(self):
        """Run all steps"""
        self.load_data()
        self.preprocess_data()
        self.split_data()
        self.train_model()

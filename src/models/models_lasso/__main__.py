from _init_ import lasso_model

if __name__ == "__main__":
    # Initialization with the path to the CSV file and the sample size
    model = lasso_model("../../../data/CO2_domicile_travail.csv", 500000)

    # Execution steps
    model.run()

#Import librarys
import pickle
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LassoCV
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error


def train_lasso_model_non_negative(y, x, filename, test_size=0.2, random_state=42, cv=10):
    """
    This function normalizes the explanatory variables, splits the data into 
    training and test sets, trains a Lasso model, and calculates the root mean 
    squared error (RMSE). It ensures that predictions do not drop below 0.  
    It also saves the trained model in pkl format.

    Args:
        y (pd.Series): Target variable.
        x (pd.DataFrame): Explanatory variables.
        filename (str): Name of the file to save the model in pkl format.
        test_size (float): Proportion of data reserved for testing (default is 0.2).
        random_state (int): Seed for reproducibility (default is 42).
        cv (int): Number of folds for cross-validation (default is 10).

    Returns:
        float: Root Mean Squared Error (RMSE) on test data.
        float: Mean Absolute Error (MAE) on test data.
    """

    # Scale the explanatory variables
    scaler = StandardScaler()
    x_normalized = scaler.fit_transform(x)

    # Split the data into training and test sets
    x_train, x_test, y_train, y_test = train_test_split(
        x_normalized, y, test_size=test_size, random_state=random_state
    )

    # Ensure that the number of folds for cross-validation is not greater than the number of samples
    cv = min(cv, len(y_train))

    # Train the Lasso model
    lasso_cv = LassoCV(cv=cv, random_state=random_state)
    lasso_cv.fit(x_train, y_train)

    # Print the best alpha value selected by LassoCV
    print(f"Meilleure valeur de alpha sélectionnée par LassoCV : {lasso_cv.alpha_}")

    # Make predictions on the test set
    y_pred = lasso_cv.predict(x_test)

    # Ensure that predictions do not drop below 0
    y_pred = np.maximum(y_pred, 0)

    # Convert the predictions and actual values to dataframes
    y_pred_df = pd.DataFrame(y_pred, columns=["donnee"]).reset_index(drop=True)
    y_test_df = y_test.to_frame(name='donnee')

    # Calculate errors
    mse = mean_squared_error(y_test_df["donnee"], y_pred_df["donnee"])
    rmse = np.sqrt(mse)
    mae = mean_absolute_error(y_test_df["donnee"], y_pred_df["donnee"])

    # Save the trained model
    with open(filename, 'wb') as file:
        pickle.dump(lasso_cv, file)

    # Return the RMSE, MAE, and the actual and predicted values
    return rmse, mae, y_test_df, y_pred_df

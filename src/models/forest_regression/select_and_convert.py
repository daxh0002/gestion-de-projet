#Import librarys
import random
import os
import pandas as pd
from tqdm import tqdm


def select_random(csv_file, sample_size=300000, chunk_size=100000):
    """
    Functionality: The function will create chunks of 100,000 individuals by default.
    It will assign each individual an index between 0 and the total number of rows in the CSV (n)
    randomly.
    If the index value is less than sample_size, then the individual is added to the DataFrame.
    It continues this way until it obtains a DataFrame with a number of individuals equal to
    sample_size.
    At the end, the function returns a DataFrame with a number of indices equal to sample_size.
    It also deletes variables that will no longer be used to free up memory.
    
    Parameters
    ----------
    csv_file : TYPE : str
        DESCRIPTION: Path to the CSV file to be processed
    sample_size : TYPE, optional
        DESCRIPTION. The default is 300000.
        Final sample size
    chunk_size : TYPE, optional
        DESCRIPTION. The default is 100000.
        Chunk size

    Returns
    -------
    TYPE DataFrame
        DESCRIPTION. DataFrame containing the selected values

    """

    sampled_rows = []  # List to store sampled rows

    # Count the total number of rows for iterations
    total_rows = sum(1 for _ in open(csv_file)) - 1  # -1 to ignore the header

    # Adjust sample_size if total_rows is less than sample_size
    if total_rows < sample_size:
        sample_size = total_rows

    # Count the total number of chunks
    total_chunks = sum(1 for _ in pd.read_csv(csv_file, chunksize=chunk_size, low_memory=False))

    # Read the file in chunks with a progress bar
    with tqdm(total=total_chunks, desc="Séléction des individus") as pbar:
        for chunk in pd.read_csv(csv_file, chunksize=chunk_size, low_memory=False):
            # Generate a random number of indices between 0 and total_rows
            random_indices = random.sample(range(total_rows), len(chunk))

            # Keep rows where the index is less than sample_size
            for i, row in enumerate(chunk.itertuples(index=False)):
                if random_indices[i] < sample_size:
                    sampled_rows.append(row)

            # Update the progress bar
            pbar.update(1)

    # Limit the number of samples to sample_size
    if len(sampled_rows) > sample_size:
        sampled_rows = random.sample(sampled_rows, sample_size)

    # Convert the list of rows to a DataFram
    sampled_df = pd.DataFrame(sampled_rows)

    # Free memory
    del sampled_rows
    del random_indices
    del chunk

    return sampled_df.reset_index(drop=True)


def split_and_sample(csv_file, output_dir, sample_size=300000, chunk_size=100000):
    """
    Splits the CSV based on the categories of the ILTUU variable and applies  
    the select_random function to extract a sample of 300,000 individuals  
    from each subset.

    Parameters
    ----------
    csv_file : str
        Path to the CSV file to be processed.
    output_dir : str
        Directory where the subsets will be saved.
    sample_size : int, optional
        Final sample size for each category (default is 300,000).
    chunk_size : int, optional
        Size of the chunks (default is 100,000).

    Returns
    -------
    None
    """
    # Read the CSV file
    df = pd.read_csv(csv_file)

    # Get the unique values of ILTUU
    unique_iltuu = df['ILTUU'].unique()

    # Create the output directory if it does not exist
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Loop through each unique value of ILTUU
    for modality in unique_iltuu:
        # Filter the DataFrame for the current modality
        modality_df = df[df['ILTUU'] == modality]

        # Save the subset to a CSV file
        modality_csv = os.path.join(output_dir, f"ILTUU_{modality}.csv")
        modality_df.to_csv(modality_csv, index=False)

        # Generate a sample for the current modality
        sampled_df = select_random(modality_csv, sample_size=sample_size, chunk_size=chunk_size)

        # Save the sampled subset to a CSV file
        sampled_csv = os.path.join(output_dir, f"ILTUU_{modality}_sampled.csv")
        sampled_df.to_csv(sampled_csv, index=False)

        print(f"Échantillon pour la modalité {modality} généré avec succès.")


variables_a_convertir = [
    'LIEU_RESID', 'LIEU_TRAV', 'MODTRANS', 'AGEREVQ', 'CS1', 'DIPL', 'EMPL', 
    'ILT', 'ILTUU', 'IMMI', 'INATC', 'INEEM', 'INPOM', 'MOCO', 'NA5', 'NPERR', 
    'SEXE', 'STAT', 'STOCD', 'TP', 'TYPL', 'TYPMR', 'VOIT', 'DEP_RESID', 
    'REG_RESID', 'CATEAAV20_RESID', 'TAAV2017_RESID', 'GRILLE_DENSITE_RESID', 
    'EPCI_EPT_RESID', 'ZE2020_RESID', 'AAV20_RESID', 'DEP_TRAV', 'REG_TRAV', 
    'CATEAAV20_TRAV', 'TAAV2017_TRAV', 'GRILLE_DENSITE_TRAV', 'EPCI_EPT_TRAV', 
    'ZE2020_TRAV', 'AAV20_TRAV'
]

def convert_to_category(df, variables = variables_a_convertir, types = 'category'):
    """
    This function converts the variables in 'variables' from our dataset  
    to the type specified in 'types'.

    Parameters
    ----------
    df : DataFrame
        DESCRIPTION: The dataframe to be modified.
    variables : list, optional
        DESCRIPTION: The list of variables to convert.  
        The default is variables_a_convertir.
    types : str, optional
        DESCRIPTION: The type to which the variables should be converted.  
        The default is 'category'.

    Returns
    -------
    None.

    """


    for var in variables:
        if var in df.columns:
            df[var] = df[var].astype(types)
        else:
            print(f"La variable '{var}' n'existe pas dans le DataFrame.")


def remove_high_variance_categories(df, variables = variables_a_convertir, threshold=15):
    """
    Removes columns from the DataFrame that have more than `threshold` unique levels.

    Args:
    df (pd.DataFrame): The original DataFrame.
    variables (list): List of columns to examine.
    threshold (int): Maximum number of allowed unique levels.

    Returns:
    pd.DataFrame: DataFrame without columns that have too many unique levels.
    """

    # List to store columns to drop
    cols_to_drop = []

    # Check each variable in the list
    for var in variables:
        if var in df.columns and df[var].nunique() > threshold:
            cols_to_drop.append(var)

    # Drop the columns and return the reduced DataFrame
    df_reduced = df.drop(columns=cols_to_drop)
    return df_reduced

variables_dummies = [
    'MODTRANS','ILTUU',
    'GRILLE_DENSITE_RESID', 'GRILLE_DENSITE_TRAV'
]


def convert_to_dummies(df, variables = variables_dummies):
    """
    Converts the specified categorical variables in the list into dummy variables.
    
    Args:
    - df (pd.DataFrame): The original dataframe.
    - variables (list): The list of columns to transform into dummies.
    
    Returns:
    - pd.DataFrame: A new dataframe with the specified variables transformed into dummies.
    """

    # Check if the specified columns are present in the dataframe
    missing_columns = [col for col in variables if col not in df.columns]
    if missing_columns:
        print(f"Les colonnes suivantes sont absentes du dataframe : {missing_columns}")
        return None

    # Convert the specified columns into dummies
    df_dummies = pd.get_dummies(df[variables], drop_first=True)

    # Drop the original columns from the dataframe
    df_rest = df.drop(columns=variables)

    # Concatenate the original dataframe with the dummy variables
    df_final = pd.concat([df_rest, df_dummies], axis=1)

    return df_final

# List of variables that are not influential
variable_not_influe = ['IPONDI','CS1','DIPL','EMPL','IMMI','INATC','INEEM',
                       'MOCO','NA5', 'NPERR','SEXE','STAT','STOCD','TP',
                       'TYPL','TYPMR','VOIT','TAAV2017_RESID','TAAV2017_TRAV','ILT' ,
                       'CATEAAV20_RESID', 'CATEAAV20_TRAV', 'REG_RESID',
                       'REG_TRAV','AGEREVQ','GRILLE_DENSITE_RESID',
                       'GRILLE_DENSITE_TRAV','ILTUU'
                       ]

def drop_variable (df, liste = variable_not_influe):
    """
    Removes the variables in the list 'liste' from the dataframe 'df'.

    Parameters
    ----------
    df : DataFrame
        Target dataframe.
    liste : list, optional
        List of variables to remove. The default is variable_not_influe.

    Returns
    -------
    df : DataFrame
        Dataframe with the specified variables removed.

    """

    df = df.drop(columns = liste)

    return df

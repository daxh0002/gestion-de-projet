# Import functions from other files.

from select_and_convert import (
    select_random,
    convert_to_category,
    remove_high_variance_categories,
    convert_to_dummies,
    drop_variable
)
from forest_building import builds_the_tree
from export_import_onnx import export_modele

class RegressionTreeModel:
    """
    A class used to create a Regression Tree Model.

    Attributes
    ----------
    filepath : str
        The path to the file containing the data.
    size : int
        The size of the dataset sample.
    df : pandas.DataFrame or None
        The dataframe containing the dataset, initialized as None.
    var_explicative : list or None
        The list of explanatory variables, initialized as None.
    var_explique : str or None
        The dependent variable, initialized as None.
    modele : object or None
        The regression tree model, initialized as None.

    Methods
    -------
    __init__(self, filepath, size)
        Initializes the RegressionTreeModel with the given file path and size.
    """

    def __init__(self, filepath, size):
        self.filepath = filepath
        self.size = size
        self.df = None
        self.var_explicative = None
        self.var_explique = None
        self.modele = None

    def load_data(self):
        """Step 1: Import the sample dataset"""
        self.df = select_random(self.filepath, self.size)

    def preprocess_data(self):
        """Step 2: Format the data"""
        convert_to_category(self.df)
        self.df = remove_high_variance_categories(self.df)
        self.df = self.df.drop(columns=['CHAMP_CO2', 'DIST_HEBDO', 'DUREE'])
        self.df = drop_variable(self.df)
        self.df = convert_to_dummies(self.df, ['MODTRANS'])
        print("Data preprocessed.")

    def split_data(self):
        """Step 3: Separate explanatory variables and target variable"""
        self.var_explicative = self.df.drop(columns=['CO2_HEBDO'])
        if 'INPOM' in self.var_explicative.columns:
            self.var_explicative.drop('INPOM', axis=1, inplace=True)
        self.var_explique = self.df['CO2_HEBDO']
        print("Data split into explanatory variables and target variable.")

    def build_model(self):
        """Step 4: Build the model"""
        self.modele = builds_the_tree(
            self.var_explicative, self.var_explique
        )
        print("Model built.")

    def export_model(self):
        """Step 5: Export the model"""
        export_modele(self.var_explicative, self.modele, "Regression_Tree")
        print("Model exported.")

    def run(self):
        """Execute all steps in order"""
        self.load_data()
        self.preprocess_data()
        self.split_data()
        self.build_model()
        self.export_model()

from _init_ import RegressionTreeModel

if __name__ == "__main__":
    # Initialization with the path to the CSV file and the sample size
    model = RegressionTreeModel("../../../data/CO2_domicile_travail.csv", 500000)

    # Execute all steps
    model.run()

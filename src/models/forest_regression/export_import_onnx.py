#Import librarys

import sys
import os
from skl2onnx import convert_sklearn
from skl2onnx.common.data_types import FloatTensorType
import pandas as pd
import sklearn
import numpy as np
import onnxruntime as rt
from typing import Union

current_dir = os.path.dirname(os.path.abspath(__file__))
os.chdir(current_dir)

def export_modele(var_explicative: pd.DataFrame,
                  modele: sklearn.base.BaseEstimator,
                  name: str) -> None:
    """
    Export a scikit-learn model to ONNX format.

    Parameters
    ----------
    var_explicative : pd.DataFrame
        Input variables for the prediction.
    modele : sklearn.base.BaseEstimator
        The scikit-learn model to be exported.
    name : str
        The name of the output ONNX file.

    Returns
    -------
    None
    """
    if var_explicative is None:
        raise ValueError("The provided DataFrame is None.")

    if not isinstance(modele, sklearn.base.BaseEstimator):
        raise ValueError("The provided model is not a valid scikit-learn estimator.")

    # Define the initial type based on the input DataFrame
    initial_type = [('float_input', FloatTensorType([None, var_explicative.shape[1]]))]

    # Convert the model to ONNX format
    onnx_model = convert_sklearn(modele, initial_types=initial_type)

    # Save the ONNX model to a file
    with open(f"{name}.onnx", "wb") as f:
        f.write(onnx_model.SerializeToString())

#Arbre_de_regression.onnx
def load_csv_data(file_path):
    """
    Loads data from a CSV file and replaces commas with dots in decimal numbers.
    """
    try:
        # Read the CSV file, replacing commas with dots
        df = pd.read_csv(
            file_path, header=None, names=["Distance", "Mode_Transport"], encoding='latin1'
            )

        # Replace commas with dots in the 'Distance' column
        df['Distance'] = df['Distance'].astype(str).str.replace(',', '.', regex=False).astype(float)

        # Uncomment the following lines to display the loaded data
        # print("Données utilisateur chargées:")
        # print(df.head())
        return df
    except Exception as e:
        # Print an error message and exit if file loading fails
        print(f"Erreur lors du chargement du fichier temporaire: {e}")
        sys.exit(1)


def use_modele(var_explicative: Union[pd.DataFrame, list], chemin: str) -> np.ndarray:
    """
    Function to use an ONNX model and make predictions.

    Parameters
    ----------
    var_explicative : pd.DataFrame or list
        Input variables for the prediction (can be a DataFrame or a list).
    chemin : str
        Path to the model.

    Returns
    -------
    np.ndarray
        Predictions from the ONNX model.
    """
    # If var_explicative is a list, convert it to the appropriate format
    if isinstance(var_explicative, list):
        distance = var_explicative[0]
        mode_transport = var_explicative[1]

        # Convert mode_transport to dummies
        dummies = [0, 0, 0, 0]
        if 2 <= mode_transport <= 5:
            dummies[mode_transport - 2] = 1

        # Combine distance with dummies
        var_explicative = [distance] + dummies
        var_explicative = pd.DataFrame([var_explicative])
    elif isinstance(var_explicative, pd.DataFrame):
        # Ensure the DataFrame has the correct columns
        if 'Distance' in var_explicative.columns and 'Mode_Transport' in var_explicative.columns:
            var_explicative['Mode_Transport'] = var_explicative['Mode_Transport'].apply(
                lambda x: [0, 0, 0, 0] if x == 1 else [1 if i == x - 2 else 0 for i in range(4)]
                )
            var_explicative = var_explicative.apply(
                lambda row: [row['Distance']] + row['Mode_Transport'], axis=1
                ).tolist()
            var_explicative = pd.DataFrame(var_explicative)
        else:
            raise ValueError("DataFrame must contain 'Distance' and 'Mode_Transport' columns")

    # Load the ONNX model
    sess = rt.InferenceSession(chemin)

    # Prepare the input data for testing
    input_name = sess.get_inputs()[0].name

    # Convert the data to a NumPy array
    x_test_onnx = np.array(var_explicative, dtype=np.float32)

    # Make predictions with the ONNX model
    y_pred_onnx = sess.run(None, {input_name: x_test_onnx})[0]
    return y_pred_onnx

if __name__ == "__main__":

    df = load_csv_data("../../../data/donnees_prediction.csv")
    # Load the ONNX model
    model_path = "Arbre_de_regression.onnx"

    # Make predictions on the loaded data
    predictions = use_modele(df[["Distance", "Mode_Transport"]], model_path)

    # Extract the first prediction, round it to two decimal places, and display it cleanly
    first_prediction = round(predictions[0][0], 2)
    print(first_prediction)

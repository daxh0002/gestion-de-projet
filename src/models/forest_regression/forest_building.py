#Import librarys
from typing import Any
import pandas as pd
import numpy as np
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, mean_absolute_error


def builds_the_tree(
    var_explicative: pd.DataFrame,
    var_explique: pd.Series,
) -> Any:
    """
    This function creates a regression tree using the parameters provided  
    by the user, tests it, displays the training and test errors,  
    and returns the created model.

    Parameters
    ----------
    var_explicative : pd.DataFrame
        Dataset containing the explanatory variables.
    var_explique : pd.Series
        Dataset containing the variable to be explained.
    random_state : int
        Random seed for the tree.
    max_depth : int
        Maximum depth of the tree.
    min_samples_split : int
        Minimum sample size required to split a node.
    min_samples_leaf : int
        Minimum sample size required at a leaf node after splitting.
    min_impurity_decrease : float
        Minimum decrease in impurity required to perform a split.

    Returns
    -------
    modele : sklearn.tree._classes.DecisionTreeRegressor
        The constructed regression tree.
    """
    # Split data
    x_train, x_test, y_train, y_test = train_test_split(
        var_explicative, var_explique, test_size=0.2, random_state=42
    )

    # Model Initialization
    regressor = DecisionTreeRegressor(
    random_state=42,
    max_depth=10,
    min_samples_split=1000,
    min_samples_leaf=500,
    min_impurity_decrease = 0.01,
    )

    # Model Train
    modele = regressor.fit(x_train, y_train)

    # Prediction
    y_pred = regressor.predict(x_test)

    # Error calculation
    mse = mean_squared_error(y_test, y_pred)
    rmse = np.sqrt(mse)
    mae = mean_absolute_error(y_test, y_pred)

    print("Pour le test : ")
    print(f"RMSE pour le modèle forest: {rmse:.2f}")
    print(f"MAE pour le modèle forest: {mae:.2f}")

    return modele

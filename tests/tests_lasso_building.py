#Import librarys from the parent directory
import sys
import os
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir)
sys.path.append('../src')

from library import *
from models.models_lasso.lasso_building import train_lasso_model_non_negative

class TestTrainLassoModelNonNegative(unittest.TestCase):

    def setUp(self):
        """
        Set up a sample DataFrame and Series for testing.
        """
        self.X = pd.DataFrame({
            'feature1': [1.0, 2.0, 3.0, 4.0, 5.0],
            'feature2': [5.0, 4.0, 3.0, 2.0, 1.0]
        })
        self.y = pd.Series([1, 2, 3, 4, 5])
        self.filename = "test_lasso_model.pkl"

    def tearDown(self):
        """
        Remove the temporary model file after each test.
        """
        if os.path.exists(self.filename):
            os.remove(self.filename)

    def test_train_lasso_model_non_negative(self):
        """
        Test the train_lasso_model_non_negative function to ensure it correctly trains and evaluates a Lasso model.
        """
        rmse, mae, y_test_df, y_pred_df = train_lasso_model_non_negative(
            y=self.y,
            X=self.X,
            filename=self.filename,
            test_size=0.2,
            random_state=42,
            cv=10
        )
        
        # Check that the model file is created
        self.assertTrue(os.path.exists(self.filename))
        
        # Check that the RMSE and MAE are floats
        self.assertIsInstance(rmse, float)
        self.assertIsInstance(mae, float)
        
        # Check that the predictions are non-negative
        self.assertTrue((y_pred_df["donnee"] >= 0).all())



if __name__ == '__main__':
    unittest.main()
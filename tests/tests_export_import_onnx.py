#Import librarys from the parent directory
import sys
import os
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir)
sys.path.append('../src')

from library import *
from models.forest_regression.export_import_onnx import export_modele, use_modele


class TestExportModele(unittest.TestCase):

    def setUp(self):
        """
        Set up a sample DataFrame and model for testing.
        """
        self.df = pd.DataFrame({
            'feature1': [1.0, 2.0, 3.0],
            'feature2': [4.0, 5.0, 6.0]
        })
        self.model = DecisionTreeRegressor()
        self.model.fit(self.df, [1, 2, 3])
        self.onnx_filename = "test_model.onnx"

    def tearDown(self):
        """
        Remove the temporary ONNX file after each test.
        """
        if os.path.exists(self.onnx_filename):
            os.remove(self.onnx_filename)

    def test_export_modele(self):
        """
        Test the export_modele function with a valid DataFrame and model.
        """
        export_modele(self.df, self.model, "test_model")
        self.assertTrue(os.path.exists(self.onnx_filename))

    def test_export_modele_invalid_model(self):
        """
        Test the export_modele function with an invalid model.
        """
        with self.assertRaises(ValueError):
            export_modele(self.df, None, "test_model")

    def test_export_modele_invalid_dataframe(self):
        """
        Test if the export_modele function raises an error with an invalid DataFrame.
        """
        with self.assertRaises(ValueError):
            export_modele(None, self.model, "test_model")



class TestUseModele(unittest.TestCase):

    def setUp(self):
        """
        Set up the path to the example ONNX model.
        """
        self.chemin = os.path.abspath('../src/models/forest_regression/Arbre_de_regression.onnx')
        print(f"Absolute path to the ONNX model: {self.chemin}")
        print(f"Current working directory: {os.getcwd()}")

    @patch('builtins.print')
    def test_use_modele_with_list(self, mock_print):
        """
        Test the use_modele function with a list input.
        """
        # Example input data
        var_explicative = [32.2, 1, 0, 0, 0]

        # Call the use_modele function
        use_modele(var_explicative, self.chemin)
        
        # Check that the function prints the expected output
        mock_print.assert_called()
        print_calls = [call.args[0] for call in mock_print.call_args_list]
        self.assertTrue(any("Prédictions ONNX :" in call for call in print_calls), "The function did not print the expected output.")


if __name__ == "__main__":
    unittest.main()
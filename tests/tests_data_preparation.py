#Import librarys from the parent directory
import sys
import os
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir)
sys.path.append('../src')

from library import *
from manip_data.data_preparation_functions import download_csv, load_csv_data, \
    clean_data_with_absence_check, convert_to_category, select_random

# Test class for the data import function
class TestLoadCSVData(unittest.TestCase):

    def setUp(self):
        """
        Prepare a temporary CSV file before each test.
        
        This method creates a small test CSV file with sample data to be used in the tests.
        """
        self.test_csv_filename = "test_data.csv"
        with open(self.test_csv_filename, "w") as f:
            f.write("col1;col2;col3\n")
            f.write("1;data1;3.0\n")
            f.write("2;data2;4.5\n")

    def tearDown(self):
        """
        Remove the temporary CSV file after each test.
        
        This method ensures that the test file is cleaned up after the test execution.
        """
        if os.path.exists(self.test_csv_filename):
            os.remove(self.test_csv_filename)

    def test_load_csv_success(self):
        """
        Test if the CSV file is loaded correctly.

        This test checks that the CSV file is read properly and that the DataFrame matches
        the expected structure.
        
        Steps:
        1. Verify the number of columns in the DataFrame.
        2. Verify the column names.
        3. Verify the data types of the columns.
        4. Verify the number of rows in the DataFrame.
        """
        df = load_csv_data(self.test_csv_filename)

        # Step 1: Verify the number of columns
        self.assertEqual(len(df.columns), 3, "The file should contain exactly 3 columns.")

        # Step 2: Verify the column names
        self.assertListEqual(
            list(df.columns),
            ["col1", "col2", "col3"],
            "The column names do not match."
            )

        # Step 3: Verify the data types of the columns
        expected_dtypes = {"col1": "int64", "col2": "object", "col3": "float64"}
        actual_dtypes = dict(df.dtypes)
        for col, dtype in expected_dtypes.items():
            self.assertEqual(
                str(actual_dtypes[col]), dtype, f"The type of column {col} is incorrect."
                )

        # Step 4: Verify the number of rows
        self.assertEqual(len(df), 2, "The file should contain exactly 2 data rows.")

        print("Test de chargement réussi : Toutes les validations ont été effectuées.")

    def test_load_csv_file_not_found(self):
        """
        Test if an exception is raised when the file does not exist.

        This test checks that the `FileNotFoundError` is raised when attempting to load
        a non-existent CSV file.
        """
        with self.assertRaises(FileNotFoundError):
            load_csv_data("invalid_path.csv")
        print("Test d'échec de chargement réussi : fichier non trouvé.")

# Test the data cleaning function
def test_clean_data_with_absence_check():
    """
    Tests the function `clean_data_with_absence_check` by validating each step of the
    data cleaning process. 
    This includes the following checks:
    
    1. Replaces commas with dots in the specified columns ('DIST_HEBDO' and 'IPONDI')
       and converts them to float type.
    2. Ensuring that the data types of the columns 'DIST_HEBDO', 'IPONDI', 'DIST', 'DUREE'
       and 'CO2_HEBDO' are correctly converted to float type.
    3. Verifying that the column 'va_abs' is properly removed after the cleaning process.
    4. Confirming that the column 'CARBU_HEBDO' is removed as expected.
    5. Ensuring that only rows with valid values in 'CO2_HEBDO' (greater than 0) are retained.
    6. Checking that there are no missing values in the 'CO2_HEBDO' column.
    
    The test provides detailed print statements at each step to confirm the correctness
    of the cleaning process.
    If all conditions are met, the test will output a success message.
    """
    # Create a test DataFrame
    data = {
        'DIST_HEBDO': ['1,0', '2,0', '3,0', '4,0', '5,0'],
        'DIST': [10, None, 15, 20, 25],
        'DUREE': [5, 10, None, 5, 10],
        'CO2_HEBDO': [100, 200, 300, 400, None],
        'CARBU_HEBDO': [50, 60, 70, 80, 90],
        'IPONDI': ['1.5', '2.5', '3.0', '4.0', '5.0']
    }
    df_test = pd.DataFrame(data)

    print("Données initiales pour le test :")
    print(df_test)

    # Apply the cleaning function
    print("\nApplication de la fonction de nettoyage...")
    cleaned_df = clean_data_with_absence_check(df_test)

    # Check the result
    print("\nVérification du nettoyage :")

    # 1. Check that commas are replaced with dots in the specified columns
    print("Vérification de la conversion des virgules en points dans les colonnes spécifiées...")
    columns_to_check = ['DIST_HEBDO', 'IPONDI']
    for column in columns_to_check:
        # Ensure the column is in string format before checking for commas
        cleaned_df[column] = cleaned_df[column].astype(str)
        assert (
            cleaned_df[column].str.contains('.').all(),
            f"Il y a des virgules non converties en points dans la colonne {column}"
            )
        print(
            f"Succès : toutes les virgules dans {column} ont été correctement converties en points"
            )

        # After replacing commas with dots, convert the column to float
        cleaned_df[column] = cleaned_df[column].str.replace(',', '.').astype(float)

    # 2. Check the column types after transformation
    print("Vérification des types de colonnes après transformation...")
    expected_columns = ['DIST_HEBDO', 'IPONDI', 'DIST', 'DUREE', 'CO2_HEBDO']
    for column in expected_columns:
        assert cleaned_df[column].dtype in [np.float64, float], f"{column} n'est pas de type float"
        print(f"Succès : {column} est bien de type float")

    # 3. Verify that the 'va_abs' column was removed after cleaning
    print("\nVérification de la suppression de la colonne 'va_abs'...")
    assert(
        'va_abs' not in cleaned_df.columns, "La colonne 'va_abs' est toujours présente"
    )
    print("Succès : la colonne 'va_abs' a été supprimée")

    # 4. Verify that the 'CARBU_HEBDO' column was removed after cleaning
    print("\nVérification de la suppression de la colonne 'CARBU_HEBDO'...")
    assert 'CARBU_HEBDO' not in cleaned_df.columns, "La colonne 'CARBU_HEBDO' est toujours présente"
    print("Succès : la colonne 'CARBU_HEBDO' a été supprimée")

    # 5. Verify that only rows with 'va_abs' = 0 and 'CO2_HEBDO' > 0 are kept
    print("\nVérification des lignes restantes avec 'CO2_HEBDO' > 0...")
    assert (cleaned_df['CO2_HEBDO'] > 0).all(), "Il y a des lignes avec 'CO2_HEBDO' <= 0"
    print("Succès : toutes les lignes ont 'CO2_HEBDO' > 0")

    # 6. Verify that there are no missing values in 'CO2_HEBDO'
    print("\nVérification des valeurs manquantes dans 'CO2_HEBDO'...")
    assert cleaned_df['CO2_HEBDO'].notnull().all(), "Il y a des valeurs manquantes dans 'CO2_HEBDO'"
    print("Succès : aucune valeur manquante dans 'CO2_HEBDO'")

    print("\nTous les tests passent avec succès !")

# Unit test for the function
class TestConvertToCategory(unittest.TestCase):

    def setUp(self):
        """This method is called before each test. It creates a base DataFrame."""
        data = {
            'LIEU_RESID': [1, 2, 3],
            'LIEU_TRAV': [4, 5, 6],
            'MODTRANS': ['A', 'B', 'C'],
            'DIPL': [10, 20, 30],
            'INVALID_VAR': [99, 88, 77]  # A variable that doesn't exist in the conversion list
        }
        self.df = pd.DataFrame(data)

    def test_convert_existing_columns(self):
        """
        Test the conversion of existing columns to 'category' type.
        
        This test checks whether the specified columns are successfully converted 
        to the 'category' type using the `convert_to_category` function.
        The columns tested here are 'LIEU_RESID', 'LIEU_TRAV', and 'MODTRANS'.
        """
        # Before conversion, we print a message indicating what we are testing
        print("\nVérification de la conversion des colonnes existantes...")

        # Call the function to convert existing columns
        convert_to_category(self.df, ['LIEU_RESID', 'LIEU_TRAV', 'MODTRANS'])

        # Check that the columns are of 'category' type and display the result
        print("\nVérification des types de colonnes après conversion...")
        assert(self.df['LIEU_RESID'].dtype == 'category',
               "La colonne 'LIEU_RESID' n'a pas été convertie en 'category'.")
        print("Succès : la colonne 'LIEU_RESID' est de type 'category'.")

        assert(self.df['LIEU_TRAV'].dtype == 'category',
               "La colonne 'LIEU_TRAV' n'a pas été convertie en 'category'.")
        print("Succès : la colonne 'LIEU_TRAV' est de type 'category'.")

        assert(self.df['MODTRANS'].dtype == 'category',
               "La colonne 'MODTRANS' n'a pas été convertie en 'category'.")
        print("Succès : la colonne 'MODTRANS' est de type 'category'.")

    def test_column_not_in_df(self):
        """
        Test to check the behavior when a column is not present in the DataFrame.
        
        This test simulates a situation where an invalid column name is passed to 
        the `convert_to_category` function. The function should raise an exception
        indicating that the column does not exist in the DataFrame.
        """
        print("\nVérification des colonnes manquantes...")

        # Capture the error message printed
        with self.assertRaises(Exception):
            with self.assertLogs() as log:
                convert_to_category(self.df, ['INVALID_VAR'])
                # Check that the expected error message is in the logs
                self.assertIn("La variable 'INVALID_VAR' n'existe pas dans le DataFrame.",
                              log.output)
                print("Succès : la colonne 'INVALID_VAR' n'existe pas dans le DataFrame.")

    def test_not_in_place_conversion(self):
        """
        Test to ensure that the function does not modify other variables.
        
        This test verifies that the `convert_to_category` function only modifies the
        specified columns, without affecting other columns in the DataFrame.
        The column 'DIPL' should not be converted to 'category' and should retain
        its original data type.
        """
        print("\nVérification des autres colonnes non modifiées...")

        # Before conversion, the type of the 'DIPL' column is not 'category'
        original_dtype = self.df['DIPL'].dtype

        # Call the conversion function for other variables
        convert_to_category(self.df, ['LIEU_RESID'])

        # Check that the 'DIPL' column has not been modified
        assert(self.df['DIPL'].dtype == original_dtype,
               "La colonne 'DIPL' a été modifiée alors qu'elle ne devait pas l'être.")
        print("Succès : la colonne 'DIPL' n'a pas été modifiée.")


# Unit test of select_random function
class TestSelectRandom(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Creating a temporary CSV file for testing
        cls.test_csv_file = 'test_data.csv'
        cls.num_rows = 1000000  # Total number of rows in the test file
        cls.df = pd.DataFrame({
            'column1': range(cls.num_rows),
            'column2': [random.random() for _ in range(cls.num_rows)]
        })
        cls.df.to_csv(cls.test_csv_file, index=False)

    @classmethod
    def tearDownClass(cls):
        # Deleting temporary CSV file after testing
        if os.path.exists(cls.test_csv_file):
            os.remove(cls.test_csv_file)

    def test_sample_size(self):
        sample_size = 300000
        sampled_df = select_random(self.test_csv_file, sample_size=sample_size)
        self.assertEqual(len(sampled_df), sample_size, "Sample size must be equal to sample_size")

    def test_unique_samples(self):
        sample_size = 300000
        sampled_df = select_random(self.test_csv_file, sample_size=sample_size)
        # Verify that samples are unique
        unique_samples = sampled_df['column1'].nunique()
        self.assertEqual(unique_samples, len(sampled_df), "All samples must be unique")

    def test_output_columns(self):
        sample_size = 300000
        sampled_df = select_random(self.test_csv_file, sample_size=sample_size)
        # Verify that the DataFrame contains the correct columns
        self.assertIn('column1', sampled_df.columns,
                      "La colonne 'column1' doit être présente dans le DataFrame")
        self.assertIn('column2', sampled_df.columns,
                      "La colonne 'column2' doit être présente dans le DataFrame")


# Unit test class for the download function
class TestCSVFunctions(unittest.TestCase):

    @patch("requests.get")
    @patch("builtins.open", new_callable=mock_open)
    def test_download_csv_success(self, mock_open_file, mock_get):
        """
        Test the successful download of a CSV file.

        This test simulates a successful HTTP request (status code 200) when downloading a CSV file. 
        It checks if the file is downloaded correctly and saved to the specified file.

        Steps:
        1. Mock a successful response from the `requests.get` call.
        2. Simulate writing the content to a file using the `open` mock.
        3. Call the `download_csv` function and verify that the file is saved with
        the expected content.
        """
        # Simulate a successful request response
        mock_response = requests.Response()
        mock_response.status_code = 200
        mock_response._content = b"mocked CSV content"
        mock_get.return_value = mock_response

        url = "http://example.com/test.csv"
        filename = "test.csv"

        # Call the function
        download_csv(url, filename)

        # Assertions
        mock_get.assert_called_once_with(url)
        mock_open_file.assert_called_once_with(filename, 'wb')
        mock_open_file().write.assert_called_once_with(b"mocked CSV content")
        print("Test de téléchargement réussi.")

    @patch("requests.get")
    def test_download_csv_failure(self, mock_get):
        """
        Test the failure of the CSV file download.

        This test simulates a failed HTTP request (status code 404) when attempting
        to download a CSV file. 
        It checks if the function raises the appropriate exception with the correct error message.

        Steps:
        1. Mock a failed request response with a 404 status code.
        2. Call the `download_csv` function and check if an exception is raised.
        """
        # Simulate a failed request response
        mock_response = requests.Response()
        mock_response.status_code = 404
        mock_get.return_value = mock_response

        url = "http://example.com/test.csv"
        filename = "test.csv"

        # Check that the exception is raised
        with self.assertRaises(Exception) as context:
            download_csv(url, filename)

        self.assertIn("Erreur lors du téléchargement du fichier : 404", str(context.exception))
    print("Test d'échec de téléchargement réussi.")


if __name__ == '__main__':
    unittest.main()

#Import librarys from the parent directory
import sys
import os
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir)
sys.path.append('../src')

from library import *
from models.forest_regression.forest_building import builds_the_tree

class TestBuildsTheTree(unittest.TestCase):

    def setUp(self):
        """
        Set up a sample DataFrame and Series for testing.
        """
        self.var_explicative = pd.DataFrame({
            'feature1': [1.0, 2.0, 3.0, 4.0, 5.0],
            'feature2': [5.0, 4.0, 3.0, 2.0, 1.0]
        })
        self.var_explique = pd.Series([1, 2, 3, 4, 5])

    def test_builds_the_tree(self):
        """
        Test the builds_the_tree function to ensure it correctly builds and evaluates a decision tree.
        """
        modele = builds_the_tree(
            var_explicative=self.var_explicative,
            var_explique=self.var_explique,
            test_size=0.2,
            random_state=42,
            max_depth=10,
            min_samples_split=2,
            min_samples_leaf=1,
            min_impurity_decrease=0.0
        )
        
        # Check that the returned model is a DecisionTreeRegressor
        self.assertIsInstance(modele, DecisionTreeRegressor)
        
        # Check that the model has been trained
        self.assertTrue(hasattr(modele, 'tree_'))

if __name__ == '__main__':
    unittest.main()
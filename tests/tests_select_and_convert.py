#Import librarys from the parent directory
import sys
import os
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir)
sys.path.append('../src')

from library import *
from models.forest_regression.select_and_convert import split_and_sample, \
    remove_high_variance_categories, variables_a_convertir,convert_to_dummies,\
    variables_dummies,variable_not_influe,drop_variable

class TestSplitAndSample(unittest.TestCase):

    def setUp(self):
        # Create a sample CSV file
        self.csv_file = 'test_sample.csv'
        self.output_dir = 'test_output'
        data = {
            'ILTUU': ['A', 'A', 'B', 'B', 'C', 'C'],
            'Value': [1, 2, 3, 4, 5, 6]
        }
        df = pd.DataFrame(data)
        df.to_csv(self.csv_file, index=False)

        # Create output directory
        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)

    def tearDown(self):
        # Remove the sample CSV file and output directory
        if os.path.exists(self.csv_file):
            os.remove(self.csv_file)
        if os.path.exists(self.output_dir):
            for file in os.listdir(self.output_dir):
                os.remove(os.path.join(self.output_dir, file))
            os.rmdir(self.output_dir)

    def test_split_and_sample(self):
        # Call the function
        split_and_sample(self.csv_file, self.output_dir, sample_size=2, chunk_size=2)

        # Check if the output files are created
        for modality in ['A', 'B', 'C']:
            modality_csv = os.path.join(self.output_dir, f"ILTUU_{modality}.csv")
            sampled_csv = os.path.join(self.output_dir, f"ILTUU_{modality}_sampled.csv")
            self.assertTrue(os.path.exists(modality_csv))
            self.assertTrue(os.path.exists(sampled_csv))

            # Check the content of the sampled files
            sampled_df = pd.read_csv(sampled_csv)
            self.assertEqual(len(sampled_df), 2)


class TestRemoveHighVarianceCategories(unittest.TestCase):

    def setUp(self):
        """
        Set up a sample DataFrame for testing.
        """
        self.df = pd.DataFrame({
            'MODTRANS': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16],
            'ILTUU': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            'GRILLE_DENSITE_RESID': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16],
            'GRILLE_DENSITE_TRAV': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16],
            'OTHER_VAR': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
        })

    def test_remove_high_variance_categories(self):
        """
        Test the remove_high_variance_categories function to ensure it removes columns with more than the specified threshold of unique values.
        """
        # Call the function with a threshold of 15
        df_reduced = remove_high_variance_categories(self.df, variables=variables_a_convertir, threshold=15)
        
        # Check that columns with more than 15 unique levels are removed
        self.assertNotIn('MODTRANS', df_reduced.columns)
        self.assertNotIn('GRILLE_DENSITE_RESID', df_reduced.columns)
        self.assertNotIn('GRILLE_DENSITE_TRAV', df_reduced.columns)
        
        # Check that columns with 15 or fewer unique levels are still present
        self.assertIn('ILTUU', df_reduced.columns)
        self.assertIn('OTHER_VAR', df_reduced.columns)


class TestConvertToDummies(unittest.TestCase):

    def setUp(self):
        """
        Set up a sample DataFrame for testing.
        """
        self.df = pd.DataFrame({
            'MODTRANS': ['car', 'bike', 'car', 'bus'],
            'ILTUU': ['A', 'B', 'A', 'C'],
            'GRILLE_DENSITE_RESID': ['high', 'low', 'medium', 'high'],
            'GRILLE_DENSITE_TRAV': ['low', 'medium', 'high', 'medium'],
            'OTHER_VAR': [1, 2, 3, 4]
        })

    def test_convert_to_dummies(self):
        """
        Test the convert_to_dummies function to ensure it correctly transforms specified categorical variables into dummy variables.
        """
        # Call the function
        df_dummies = convert_to_dummies(self.df, variables=variables_dummies)
        
        # Check that the original columns are removed
        for var in variables_dummies:
            self.assertNotIn(var, df_dummies.columns)
        
        # Check that the dummy columns are added
        expected_dummy_columns = [
            'MODTRANS_bus', 'MODTRANS_car',
            'ILTUU_B', 'ILTUU_C',
            'GRILLE_DENSITE_RESID_low', 'GRILLE_DENSITE_RESID_medium',
            'GRILLE_DENSITE_TRAV_low', 'GRILLE_DENSITE_TRAV_medium'
        ]
        for col in expected_dummy_columns:
            self.assertIn(col, df_dummies.columns)
        
        # Check that the non-transformed columns are still present
        self.assertIn('OTHER_VAR', df_dummies.columns)

    def test_convert_to_dummies_missing_columns(self):
        """
        Test the convert_to_dummies function to ensure it handles missing columns correctly.
        """
        # Remove a column from the DataFrame
        df_missing = self.df.drop(columns=['MODTRANS'])
        
        # Call the function
        df_dummies = convert_to_dummies(df_missing, variables=variables_dummies)
        
        # Check that the function returns None
        self.assertIsNone(df_dummies)



class TestDropVariable(unittest.TestCase):

    def setUp(self):
        """
        Set up a sample DataFrame for testing.
        """
        self.df = pd.DataFrame({
            'IPONDI': [1, 2, 3],
            'CS1': [1, 2, 3],
            'DIPL': [1, 2, 3],
            'EMPL': [1, 2, 3],
            'IMMI': [1, 2, 3],
            'INATC': [1, 2, 3],
            'INEEM': [1, 2, 3],
            'MOCO': [1, 2, 3],
            'NA5': [1, 2, 3],
            'NPERR': [1, 2, 3],
            'SEXE': [1, 2, 3],
            'STAT': [1, 2, 3],
            'STOCD': [1, 2, 3],
            'TP': [1, 2, 3],
            'TYPL': [1, 2, 3],
            'TYPMR': [1, 2, 3],
            'VOIT': [1, 2, 3],
            'TAAV2017_RESID': [1, 2, 3],
            'TAAV2017_TRAV': [1, 2, 3],
            'ILT': [1, 2, 3],
            'CATEAAV20_RESID': [1, 2, 3],
            'CATEAAV20_TRAV': [1, 2, 3],
            'REG_RESID': [1, 2, 3],
            'REG_TRAV': [1, 2, 3],
            'AGEREVQ': [1, 2, 3],
            'GRILLE_DENSITE_RESID': [1, 2, 3],
            'GRILLE_DENSITE_TRAV': [1, 2, 3],
            'ILTUU': [1, 2, 3],
            'OTHER_VAR': [1, 2, 3]
        })

    def test_drop_variable(self):
        """
        Test the drop_variable function to ensure it correctly drops specified columns.
        """
        df_reduced = drop_variable(self.df, liste=variable_not_influe)
        
        # Check that the specified columns are removed
        for var in variable_not_influe:
            self.assertNotIn(var, df_reduced.columns)
        
        # Check that the non-specified columns are still present
        self.assertIn('OTHER_VAR', df_reduced.columns)
        
if __name__ == '__main__':
    unittest.main()
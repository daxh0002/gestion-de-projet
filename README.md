# CARBOMILES, the application that predicts weekly greenhouse gas emissions per person for commuting to work 


## Project goals 🎯

The aim of this project is to develop an application that enables users to estimate their weekly greenhouse gas emissions during their home-to-work journeys. To achieve this, we have developed a predictive model based on greenhouse gas emissions data for commutes of less than 100 km in mainland France. The data were retrieved from the platform [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/estimation-des-emissions-individuelles-de-gaz-a-effet-de-serre-lors-des-deplacements-domicile-travail/). 


## Repository structure 🗃️

The repository is structured as follows : 

- 📁 The `data` directory includes the Excel files in `.csv` format from our project, namely the original database, the cleaned database and the data entered by the user in the form used for prediction.
- 📁 The `docs` directory contains our project's final report in `.pdf` format.
- 📁 The `interface_vba` directory contains an Excel file in `.xlsm` format, corresponding to the application's user interface. 
- 📁 The `src` directory contains all the Python scripts for data science and data manipulation. This directory is divided into 4 sub-folders :
    - 📂 The `manip_data` subfolder, which includes Python files reserved for data preparation and cleaning ;
    - 📂 The `models` subfolder, which contains the Python scripts used to build the prediction model and select significant variables ;
    - 📂 The `pipeline_vba` subfolder, which contains the Python scripts used to link VBA to Python ;
    - 📂 The `visualization` subfolder, which includes Python files designed to build graphics to be inserted into the application.
- 📁 The `tests` directory contains all the unit tests performed on our Python scripts to ensure that they work properly and improve code quality. 
- 📄 The `README.md` file describes the organization and use of the project.
- 📄 The `requirements.txt` file lists the Python packages and their versions required to run the application.

In the project's [wiki](https://gitlab-mi.univ-reims.fr/daxh0002/gestion-de-projet/-/wikis/pages), you'll also find the dictionary of database variables, completed sprints and the project roadmap.  


## Configuration 🧰

### Prerequisites
First of all, make sure you have Python installed on your machine. If not, you can download it from the official Python website [python.org](https://www.python.org).

Make sure you also have Microsoft Excel installed to be able to use our interface.

### Installation
1. Clone this GitLab repository on your local machine by running this in your terminal :
```
git clone https://gitlab-mi.univ-reims.fr/daxh0002/gestion-de-projet.git
```
2. Open the terminal and go to the root of the folder you've just cloned to install the required dependencies. To do this, insert the following command :
```
python -m pip install -r requirements.txt
```


## How to use this project 🔎

To use our application, simply follow these steps : 

1. Launch the application via the Excel interface

    - Go to the `interface_vba` directory.
    - Launch the `formulaire_co2.xlsm` file to open the application.
      To ensure correct operation of the application, you need to authorize files with macros. To do this, right-click on the file concerned, go to properties, then at the very bottom of the General tab check the Unblock box.
    
2. Enter information in the form

    - Press the « Estimez votre empreinte carbone » button to open the associated form.
    - Fill in the various fields on the form.

3. Predicting weekly CO2 emissions

    - Once you've entered your commuting information, press the « Valider » button.
    - The prediction model, written in Python, will automatically run in the background and predict your carbon impact based on the information you've entered in the form. 

4. Displaying results 
    
    - You'll be asked to wait a few seconds while your estimated weekly CO2 emissions are calculated.
    - Once the estimate is complete, a dashboard is displayed, showing your carbon footprint and additional visualization elements for a more personalized experience.


## Contributors 💪🏼

This application was developed by students in the « Statistique pour l’Évaluation et la Prévision » (SEP) master's program at the University of Reims Champagne-Ardenne (class of 2024-2025). 

Below is the list of contributors to this project and their roles : 
- Brieuc DAXHELET : Product Owner 
- Axel DUBREUIL : Data Analyst/Data Governance and Scrum Master 
- Moussa DIALLO : Data Engineer 
- Imane SAHNOUNE : Data Scientist
- Emilie LI : Front End/User Interface
